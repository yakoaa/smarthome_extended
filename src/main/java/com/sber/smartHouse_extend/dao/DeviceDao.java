package com.sber.smartHouse_extend.dao;

import com.sber.smartHouse_extend.model.Device;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceDao extends CrudRepository<Device, Long> {

    Device findByName(String name);
}
