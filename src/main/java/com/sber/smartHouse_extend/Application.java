package com.sber.smartHouse_extend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableJpaRepositories("com.sber.smartHouse_extend.dao")
@EnableScheduling
public class Application {
    public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
}
}
