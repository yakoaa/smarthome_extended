package com.sber.smartHouse_extend.controller;

import com.sber.smartHouse_extend.model.Device;
import com.sber.smartHouse_extend.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/smart-home/extend/api/device")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    //добавление устройства
    @PostMapping(value = "/add", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getDevByName(@RequestBody Device device){
        deviceService.update(device);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    //смена статуса устройства
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getDevByName(@PathVariable Long id){
        Device device = deviceService.getDeviceById(id).get();
        if (device.getState().equals("Enabled")){
            device.setState("Disabled");
            deviceService.sendLog(device.getName(),"Disabled");
        } else{
            device.setState("Enabled");
            deviceService.sendLog(device.getName(),"Enabled");
        }
        deviceService.update(device);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }
}
