package com.sber.smartHouse_extend.batch;

import com.sber.smartHouse_extend.service.DeviceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class DeviceLogBatchBean {

    @Autowired
    private DeviceService deviceService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Async
    @Scheduled(fixedDelay = 60000, initialDelay = 60000)
    public void cronJob(){
        logger.info("> cron");
        deviceService.sendRandomLogs();
    }
}
