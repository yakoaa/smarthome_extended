package com.sber.smartHouse_extend.service;

import com.sber.smartHouse_extend.dao.DeviceDao;
import com.sber.smartHouse_extend.model.Device;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Optional;
import java.util.Random;

@Service
public class DeviceService {

    @Autowired
    private DeviceDao deviceDao;


    @Autowired
    private KafkaProducer kafkaProducer;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public ArrayList<Device> getDevices(){
        return (ArrayList<Device>) deviceDao.findAll();
    }

    public Device getOneByName(String name){return deviceDao.findByName(name);}

    public Optional<Device> getDeviceById(Long id){
        return deviceDao.findById(id);
    }

    public void update(Device device){
        deviceDao.save(device);
    }

    public void sendRandomLogs(){
        Clock clock = Clock.system(ZoneId.of("Europe/Moscow"));
        ArrayList<Device> devices = (ArrayList<Device>) deviceDao.findAll();
        long start = System.currentTimeMillis();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(clock.instant(), ZoneId.of("Europe/Moscow"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        String formattedDateTime = localDateTime.format(formatter);
        Random random = new Random(start);
        for(Device device: devices){
            if(device.getState().equals("Enabled") && device.getType().equals("SENSOR")) {
                String message = String.format("%s#%s#%d#%s is send his current state#%s", device.getName(), device.getState(), random.nextInt(100)+1, device.getName(), formattedDateTime);
                kafkaProducer.sendMessage(message);
                logger.info(message);
            }
        }
    }

    public void sendLog(String name, String state){
        Clock clock = Clock.system(ZoneId.of("Europe/Moscow"));
        LocalDateTime localDateTime = LocalDateTime.ofInstant(clock.instant(), ZoneId.of("Europe/Moscow"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        String formattedDateTime = localDateTime.format(formatter);
        String message = String.format("%s#%s#%d#%s is now in state %s#%s", name, state, 0, name, state, formattedDateTime);
        kafkaProducer.sendMessage(message);
        logger.info(message);
    }


}
